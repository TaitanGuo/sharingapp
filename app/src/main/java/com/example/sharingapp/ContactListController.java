package com.example.sharingapp;

import android.content.Context;

import java.util.ArrayList;

public class ContactListController {
    private ContactList contact_list;

    public ContactListController(ContactList contact_list) {
        this.contact_list = contact_list;
    }

    public void setContacts(ArrayList<Contact> contact_list) {
        this.contact_list.setContacts(contact_list);
    }

    public ArrayList<Contact> getContacts() {
        return this.contact_list.getContacts();
    }

    public boolean addContact(Contact contact, Context context) {
        AddContactCommand command = new AddContactCommand(contact_list, contact, context);
        command.execute();
        return command.isExecuted();
    }

    public boolean deleteContact(Contact contact, Context context) {
        DeleteContactCommand command = new DeleteContactCommand(contact_list, contact, context);
        command.execute();
        return command.isExecuted();
    }

    public boolean editContact(Contact oldContact, Contact newContact, Context context) {
        EditContactCommand command = new EditContactCommand(contact_list, oldContact, newContact, context);
        command.execute();
        return command.isExecuted();
    }

    public Contact getContact(int index) {
        return this.contact_list.getContact(index);
    }

    public int getSize() {
        return this.contact_list.getSize();
    }

    public Contact getContactByUsername(String username) {
        return this.contact_list.getContactByUsername(username);
    }

    public boolean hasContact(Contact contact) {
        return this.contact_list.hasContact(contact);
    }

    public int getIndex(Contact contact) {
        return this.contact_list.getIndex(contact);
    }

    public boolean isUsernameAvailable(String username) {
        return this.contact_list.isUsernameAvailable(username);
    }

    public ArrayList<String> getAllUsernames() {
        return this.contact_list.getAllUsernames();
    }

    public void loadContacts(Context context) {
        this.contact_list.loadContacts(context);
    }

    public void addObserver(Observer observer) {
        this.contact_list.addObserver(observer);
    }

    public void removeObserver(Observer observer) {
        this.contact_list.removeObserver(observer);
    }
}
